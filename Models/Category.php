<?php

class Category
{
    public $categories;

    public function __construct($conn)
    {
        $this->fetchCategories($conn);
    }

    public function fetchCategories($conn)
    {
        $result = $conn->query('select * from categories');

        while ($row = mysqli_fetch_array($result)) {
            $this->categories[$row['id']] = $row;
        }
    }

    public function makeCategoriesMap()
    {
        $children = array();
        foreach ($this->categories as $category) {
            $children[$category['id']] = $category['parent_id'];
        }

        foreach ($this->categories as $category) {
            if (!$category['parent_id']) {
                $this->printCategory($category, $children);
            }
        }
    }

    public function printCategory($category, $children)
    {
        echo '<li>';
        echo $category['name'];
        $ul = false;
        while (true) {
            $key = array_search($category['id'], $children);
            if (!$key) {
                if ($ul) {
                    echo '</ul>';
                }
                break;
            }
            unset($children[$key]);
            if (!$ul) {
                echo '<ul>';
                $ul = true;
            }

            $this->printCategory($this->categories[$key], $children);
        }
        echo '</li>';
    }
}
